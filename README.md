# [Cython](https://pypi.org/project/Cython/)

The Cython compiler for writing C extensions for the Python language. https://cython.org/

* https://gitlab.com/debian-packages-demo/cython
* https://gitlab.com/alpinelinux-packages-demo/cython


* http://docs.cython.org/en/latest/src/quickstart/install.html
* https://en.wikipedia.org/wiki/Cython